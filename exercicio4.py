class ControlarDesconto(object):

    def __init__(self):
        self.__desconto = None
        self.__preco = None

    @property
    def desconto(self):
        return self.__desconto

    @property
    def preco(self):
        return self.__preco


    @desconto.setter
    def desconto(self, val__desconto):
        if val__desconto > 1 and val__desconto < 35:
            self.__desconto = val__desconto

        else:
            self.__desconto = 1
            print('Desconto invalido. Valor foi alterado para o padrão, igual a 1%')


    def novoPreco(self, preco, desconto):
        novoPreco = float((preco - ((desconto/100)*preco)))
        return novoPreco


print()
print('Entre com as informações do Produto: ')
preco = float(input("Digite o preco do produto: "))
desconto = int(input("Digite o desconto em % do produto: "))
print()


calc = ControlarDesconto()

calc.val__preco = preco
print("Preco:",calc.val__preco)
calc.val__desconto = desconto
print("Desconto:",calc.val__desconto)
print("Novo Preco:",calc.novoPreco(preco, desconto))

